from dropgen.RDSTable import RDSTable
from dropgen.RDSItem import RDSItem 

from tests.TestCaseTables import *

class LootDemo():

    def unique_potion(self):
        '''
        The drop will always drop ONE potion
        '''
        parent_table = RDSTable(count=5)

        parent_table.add_entry(TestCasePotionTable(unique=True, always=True))
        parent_table.add_entry(TestCaseArmorTable())
        parent_table.add_entry(TestCaseSwordTable())
        parent_table.add_entry(TestCaseCommonTable(), probability=5)
        parent_table.add_entry(TestCaseGoldTable(unique=True))

        results = parent_table.rds_result

        print("Drop will contain one potion.")
        for result in results:
            print(f"\t{result}")

    def always_drop_at_least_one_gold(self):
        '''
        Drop at least one piece of gold
        '''
        parent_table = RDSTable(count=10)

        parent_table.add_entry(TestGoldValue(1, 1), always=True)
        parent_table.add_entry(TestCaseCommonTable(), probability=5)
        parent_table.add_entry(TestCasePotionTable(probability=2))

        results = parent_table.rds_result

        print("Drop will contain at least one piece of gold.")
        for result in results:
            print(f"\t{result}")

    def drops_probabilities_equal(self):
        '''
        The drop could contain anything.
        '''
        parent_table = RDSTable(count=5)

        parent_table.add_entry(TestCasePotionTable())
        parent_table.add_entry(TestCaseArmorTable())
        parent_table.add_entry(TestCaseSwordTable())
        parent_table.add_entry(TestCaseCommonTable())
        parent_table.add_entry(TestCaseGoldTable())

        parent_table.add_entry(RDSItem("Epic -- Blood ruby"))
        parent_table.add_entry(RDSItem("Epic -- Platinum Armor"))

        results = parent_table.rds_result

        print("Drop could contain anything, the probability is the same for everything.")
        for result in results:
            print(f"\t{result}")

    def drops_with_probabilities_enabled(self):
        '''
        What drops will be based on its probability
        '''
        parent_table = RDSTable(count=5)

        parent_table.add_entry(TestCaseCommonTable(), 60)
        parent_table.add_entry(TestCasePotionTable(), 5)
        parent_table.add_entry(TestCaseArmorTable(), 5)
        parent_table.add_entry(TestCaseSwordTable(), 7)
        parent_table.add_entry(TestCaseGoldTable(), 7)

        parent_table.add_entry(RDSItem("Epic -- Blood ruby"), .5)
        parent_table.add_entry(RDSItem("Epic -- Platinum Armor"), 1)

        results = parent_table.rds_result

        print("What drops will be based on probability")
        for result in results:
            print(f"\t{result}")

    




if __name__ == '__main__':
    loot_demo = LootDemo()
    loot_demo.drops_probabilities_equal()
    loot_demo.drops_with_probabilities_enabled()
    loot_demo.unique_potion()
    loot_demo.always_drop_at_least_one_gold()