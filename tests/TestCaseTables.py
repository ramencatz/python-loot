from dropgen.RDSTable import RDSTable
from dropgen.RDSNullValue import RDSNullValue
from dropgen.RDSItem import RDSItem

from tests.TestCaseItems import TestGoldValue

class TestCasePotionTable(RDSTable):

    def __init__(self, unique=None, always=None, probability=None):
        RDSTable.__init__(self, unique=unique, always=always, probability=probability)

        self.add_entry(RDSItem("Potion - Red Potion"))
        self.add_entry(RDSItem("Potion - Green Potion"))
        self.add_entry(RDSItem("Potion - Red Potion"))
        self.add_entry(RDSItem("Potion - Multi-color potion"))




class TestCaseArmorTable(RDSTable):

    def __init__(self, unique=None, always=None, probability=None):
        RDSTable.__init__(self, unique=unique, always=always, probability=probability)

        self.add_entry(RDSItem("Armor - Chainmail"))
        self.add_entry(RDSItem("Armor - Plated Armor"))
        self.add_entry(RDSItem("Armor - Silver gauntlets"))
        self.add_entry(RDSItem("Armor - Gold metal boots"))



class TestCaseSwordTable(RDSTable):
    def __init__(self, unique=None, always=None, probability=None):
        RDSTable.__init__(self, unique=unique, always=always, probability=probability)

        self.add_entry(RDSItem("Sword - Two handed sword"))
        self.add_entry(RDSItem("Sword - Rapier"))
        self.add_entry(RDSItem("Sword - Short sword"))
        self.add_entry(RDSItem("Sword - Lancet"))



class TestCaseGoldTable(RDSTable):
    def __init__(self, unique=None, always=None, probability=None):
        RDSTable.__init__(self, unique=unique, always=always, probability=probability)

        self.add_entry(TestGoldValue(25, 10))
        self.add_entry(TestGoldValue(10, 20))
        self.add_entry(TestGoldValue(15, 15))
        self.add_entry(TestGoldValue(1, 50))

class TestCaseCommonTable(RDSTable):
    def __init__(self, unique=None, always=None, probability=None):
        RDSTable.__init__(self, unique=unique, always=always, probability=probability)

        self.add_entry(RDSItem("Common - rock"))
        self.add_entry(RDSItem("Common - rusty knife"))
        self.add_entry(RDSItem("Common - ham sandwich"))
        self.add_entry(RDSItem("Common - feral cat"))

    

