from dropgen.RDSObject import RDSObject
from dropgen.RDSValue import RDSValue

class TestCaseItem(RDSObject):

    def __init__(self, name, probability=None, unique=None, always=None, enabled=None):
        self.name = name
        RDSObject.__init__(self, probability, unique, always, enabled)


    def on_rds_hit(self):
        #print(f"{ self.name } was hit!")
        pass

    def __str__(self):
        return self.name

class TestGoldValue(RDSValue):

    def __init__(self, probability, value, unique=None, always=None, enabled=None ):
        RDSValue.__init__(self, probability, value, unique=unique, always=always, enabled=enabled)

    def __str__(self):
        return f"{ self.rds_value } pieces of gold"

