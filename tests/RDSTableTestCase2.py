from dropgen.RDSTable import RDSTable
from dropgen.RDSNullValue import RDSNullValue 

from tests.TestCaseItems import TestCaseItem
from tests.TestCaseTables import *

from unittest import TestCase

class RDSTableTestCase2(TestCase):
    '''
    Test out the RDSTable
    '''

    def setUp(self):
        '''
        Initialize the RDSTable -- parent table
        '''
        self.rdstable = RDSTable(count=5, probability=10)

    
    def test_get_result_unique_item(self):
        '''
        Test that an item is dropped only once
        '''
        unique_item = TestCaseItem("Unique item", probability=50, unique=True)
        item1 = TestCaseItem("Armor", probability=10)
        item2 = TestCaseItem("Rock", probability=25)

        self.rdstable.add_entry(unique_item)
        self.rdstable.add_entry(item1)
        self.rdstable.add_entry(item2)

        results = self.rdstable.rds_result

        #
        # Count how often unique item shows up
        #
        count = 0
        for result in results:
            if result.name == "Unique item":
                count += 1
        
        print(f"Drops returned: { len(results) }")
        self.assertTrue((count <= 1), f"The item should have dropped only once and not { count } times")

    def test_get_result_make_table_unique(self):
        self.rdstable.add_entry(TestCasePotionTable(), unique=True)
        self.rdstable.add_entry(TestCaseArmorTable())
        self.rdstable.add_entry(TestCaseSwordTable())

        results = self.rdstable.rds_result

        #
        # Count how many potions
        #
        count = 0
        for result in results:
            if result.name.find("Potion") >= 0:
                count += 1
   

        print(f"Drops returned: { len(results) }")
        self.assertTrue((count <= 1), f"Only one potion should have dropped and not { count } times")

    def test_get_result_make_table_always(self):
        self.rdstable.add_entry(TestCasePotionTable())
        self.rdstable.add_entry(TestCaseArmorTable(), always=True)
        self.rdstable.add_entry(TestCaseSwordTable())

        results = self.rdstable.rds_result

        #
        # Count how much armor was returned.  At least one needs to drop
        #
        count = 0
        for result in results:
            if result.name.find("Armor") >= 0 :
                count += 1

        print(f"Drops returned: { len(results) }")
        self.assertTrue((count >= 1), f"At least one armor should have dropped and not { count }")


    def test_get_result_make_table_always_unique(self):
        self.rdstable.add_entry(TestCasePotionTable())
        self.rdstable.add_entry(TestCaseArmorTable())
        self.rdstable.add_entry(TestCaseSwordTable(), always=True, unique=True)

        results = self.rdstable.rds_result

        #
        # Count how many swords were returned.  ONLY 1 should drop 
        #
        count = 0
        for result in results:
            if result.name.find("Sword") >= 0 :
                count += 1

        print(f"Drops returned: { len(results) }")
        self.assertTrue((count >= 1), f"ONLY 1 sword should have dropped and not { count }")

    def test_get_results_mix_items_and_tables(self):
        self.rdstable.add_entry(TestCaseItem("Horse"))
        self.rdstable.add_entry(TestCaseItem("Saddle"))
        self.rdstable.add_entry(TestCaseItem("Rock"))

        self.rdstable.add_entry(TestCasePotionTable())
        self.rdstable.add_entry(TestCaseArmorTable())
        self.rdstable.add_entry(TestCaseSwordTable())

        results = self.rdstable.rds_result
        print(f"Drops returned: { len(results) }")
        self.assertTrue((len(results)) > 0, "Items must be returned in the result set")



if __name__ == '__main__':
    unittest.main()

