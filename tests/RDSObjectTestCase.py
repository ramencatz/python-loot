from dropgen.RDSObject import RDSObject
from dropgen.RDSTable import RDSTable

from unittest import TestCase

class RDSObjectTestCase(TestCase):
    '''
    Test out RDSObjectTestCase
    '''
    def setUp(self):
        '''
        Initialize opposite of the defaults
        '''
        self.rdsobject = RDSObject(probability=5, enabled=False, always=True, unique=True)

    def test_init_basic(self):
        '''
        Successfully initialize a minimal RDSObject object
        '''
        rdsobject_test = RDSObject()

        self.assertEqual(rdsobject_test.rds_probability, 1, f"Probability should equal 1 and not { rdsobject_test.rds_probability }")
        self.assertTrue(rdsobject_test.rds_enabled, "'enabled' should be True")
        self.assertFalse(rdsobject_test.rds_always, "'always' should be False")
        self.assertFalse(rdsobject_test.rds_unique, "'unique' should be False")
    
    def test_init_with_all(self):
        '''
        Test setup where settings are opposite of the default
        '''

        self.assertEqual(self.rdsobject.rds_probability, 5, f"Probability should equal 5 and not { self.rdsobject.rds_probability }")
        self.assertFalse(self.rdsobject.rds_enabled, "'enabled' should be False")
        self.assertTrue(self.rdsobject.rds_always, "'always' should be True")
        self.assertTrue(self.rdsobject.rds_unique, "'unique' should be True")

    
    def test_rdstable(self):
        '''
        Set the RDSTable property
        '''

        self.rdsobject.rds_table = RDSTable()
        self.assertIsInstance(self.rdsobject.rds_table, RDSTable)


if __name__ == '__main__':
    unittest.main()