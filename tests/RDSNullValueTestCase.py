from dropgen.RDSNullValue import RDSNullValue
from unittest import TestCase

class RDSNullValueTestCase(TestCase):
    '''
    Test out the RDSNullValue 
    '''

    def setUp(self):
        '''
        Initialize the null value 
        '''
        self.rdsnullvalue = RDSNullValue(1)

    def test_value_is_none(self):
        '''
        The value is None
        '''
        self.assertEqual(self.rdsnullvalue.rds_value, None, "The value should be None")

if __name__ == '__main__':
    unittest.main()