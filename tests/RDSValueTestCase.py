from dropgen.RDSValue import RDSValue
from dropgen.RDSTable import RDSTable

from unittest import TestCase

class RDSValueTestCase(TestCase):
    '''
    Test out the RDSValue 
    '''

    def setUp(self):
        '''
        Initialize opposite of the defaults
        '''
        self.rdsvalue = RDSValue(1, "test value", enabled=False, always=True, unique=True)

    def test_init_basic(self):
        '''
        Successfully initialize a minimal RDSValue object
        '''
        rdsvalue_test = RDSValue(1, "test value")

        self.assertEqual(rdsvalue_test.rds_probability, 1, f"Probability should equal 1 and not { rdsvalue_test.rds_probability }")
        self.assertEqual(rdsvalue_test.rds_value, "test value", "Value should be equal to 'test value'")
        self.assertTrue(rdsvalue_test.rds_enabled, "'enabled' should be True")
        self.assertFalse(rdsvalue_test.rds_always, "'always' should be False")
        self.assertFalse(rdsvalue_test.rds_unique, "'unique' should be False")

    def test_init_with_all(self):
        '''
        Test setup where settings are opposite of the default
        '''

        self.assertEqual(self.rdsvalue.rds_probability, 1, f"Probability should equal 1 and not { self.rdsvalue.rds_probability }")
        self.assertEqual(self.rdsvalue.rds_value, "test value", "Value should be equal to 'test value'")
        self.assertFalse(self.rdsvalue.rds_enabled, "'enabled' should be False")
        self.assertTrue(self.rdsvalue.rds_always, "'always' should be True")
        self.assertTrue(self.rdsvalue.rds_unique, "'unique' should be True")

    
    def test_rdstable(self):
        '''
        Set the RDSTable property
        '''

        self.rdsvalue.rds_table = RDSTable()
        self.assertIsInstance(self.rdsvalue.rds_table, RDSTable)


    




if __name__ == '__main__':
    unittest.main()
