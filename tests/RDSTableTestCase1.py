from dropgen.RDSTable import RDSTable
from dropgen.RDSNullValue import RDSNullValue 

from tests.TestCaseItems import TestCaseItem

from unittest import TestCase

class RDSTableTestCase1(TestCase):
    '''
    Test out the RDSTable
    '''

    def setUp(self):
        '''
        Initialize the RDSTable -- all defaults
        '''
        self.rdstable = RDSTable()

    #
    # Test default instantiation values
    #
    
    def test_default_table_contents(self):
        '''
        Make sure the table is empty
        '''
        self.assertEqual(len(self.rdstable.rds_contents), 0, "Table should have a length of 0")

    def test_default_table_count(self):
        '''
        Make sure that the number of items that can drop is 1
        '''
        self.assertEqual(self.rdstable.rds_probability, 1, "Probability should be 1")

    def test_default_table_unique(self):
        '''
        Make sure that unique is set to False
        '''
        self.assertFalse(self.rdstable.rds_unique, "Unique should be False")

    def test_default_table_always(self):
        '''
        Make sure that always (drops) is set to False
        '''
        self.assertFalse(self.rdstable.rds_always, "Always should be False")

    def test_default_table_enabled(self):
        '''
        Make sure that enabled is set to True
        '''
        self.assertTrue(self.rdstable.rds_enabled, "Enabled should be True")

    #
    # Manage contents
    #
    def test_add_entry(self):
        '''
        Validate that a new IRDSObject is added to the table
        '''
        new_table = RDSTable()

        self.rdstable.add_entry(new_table)

        self.assertEqual(len(self.rdstable.rds_contents), 1, "Table should have a length of 1")
        self.assertIsInstance(new_table.rds_table, RDSTable)
    
    def test_add_entry_with_probability(self):
        '''
        Validate that a new IRDSObject is added to the table with its own probability
        '''
        new_table = RDSTable()

        self.rdstable.add_entry(new_table, probability=3.155)

        self.assertEqual(len(self.rdstable.rds_contents), 1, "Table should have a length of 1")
        self.assertIsInstance(new_table.rds_table, RDSTable)
        self.assertEqual(new_table.rds_probability, 3.155, f"The probability should be 3.155 and not { new_table.rds_probability }")

    def test_add_entry_with_unique(self):
        '''
        Validate that a new IRDSObject is added to the table with its own unique setting
        '''
        new_table = RDSTable()

        self.rdstable.add_entry(new_table, unique=False)

        self.assertEqual(len(self.rdstable.rds_contents), 1, "Table should have a length of 1")
        self.assertIsInstance(new_table.rds_table, RDSTable)

    def test_add_entry_with_always(self):
        '''
        Validate that a new IRDSObject is added to the table with its own always setting
        '''
        new_table = RDSTable()

        self.rdstable.add_entry(new_table, always=False)

        self.assertEqual(len(self.rdstable.rds_contents), 1, "Table should have a length of 1")
        self.assertIsInstance(new_table.rds_table, RDSTable)

    def test_add_entry_with_enabled(self):
        '''
        Validate that a new IRDSObject is added to the table with its own enabled setting
        '''
        new_table = RDSTable()

        self.rdstable.add_entry(new_table, enabled=False)

        self.assertEqual(len(self.rdstable.rds_contents), 1, "Table should have a length of 1")
        self.assertIsInstance(new_table.rds_table, RDSTable)

    def test_remove_entry(self):
        '''
        Remove an entry from the table
        '''
        new_table1 = RDSTable()
        new_table2 = RDSTable()
        self.rdstable.add_entry(new_table1)
        self.rdstable.add_entry(new_table2)

        self.assertEqual(len(self.rdstable.rds_contents), 2, "Table should have a length of 2")

        self.rdstable.remove_entry(new_table1)
        self.assertEqual(len(self.rdstable.rds_contents), 1, "Table should have a length of 1 after removal")

    #
    # Generate the results 
    # 
    def test_get_result_basic_recursion(self):

        self.rdstable.add_entry(RDSTable(always=True,enabled=True))
        self.rdstable.add_entry(RDSTable(always=True,enabled=True))
        self.rdstable.add_entry(RDSTable(always=True,enabled=False))
        self.rdstable.add_entry(RDSTable(always=False,enabled=True))

        self.assertIsNotNone(self.rdstable.rds_result)

    def test_get_result_with_always_items(self):
        item1 = TestCaseItem("Green Cloth", always=True)
        item2 = TestCaseItem("Rusty Sword", always=True)
        item3 = TestCaseItem("Wet Mop", always=True)

        self.rdstable.add_entry(item1)
        self.rdstable.add_entry(item2)
        self.rdstable.add_entry(item3)

        self.assertIsNotNone(self.rdstable.rds_result)
        self.assertEqual(len(self.rdstable.rds_result), 3, "There should be 3 drops")

    def test_get_result_two_always_items(self):
        item1 = TestCaseItem("Green Cloth", always=True)
        item2 = TestCaseItem("Rusty Sword", always=True)
        item3 = TestCaseItem("Wet Mop", always=False)

        self.rdstable.add_entry(item1)
        self.rdstable.add_entry(item2)
        self.rdstable.add_entry(item3)

        self.assertIsNotNone(self.rdstable.rds_result)
        self.assertEqual(len(self.rdstable.rds_result), 2, "There should be 2 drops")

    def test_get_results_where_there_is_a_null_value(self):

        item1 = TestCaseItem("Green Cloth", always=True)
        item2 = TestCaseItem("Rusty Sword", always=True)
        item3 = TestCaseItem("Wet Mop", always=True)
        nullvalue = RDSNullValue(1) #probability is required
        nullvalue.rds_always = True

        self.rdstable.add_entry(item1)
        self.rdstable.add_entry(item2)
        self.rdstable.add_entry(item3)
        self.rdstable.add_entry(nullvalue)

        self.assertEqual(len(self.rdstable.rds_result), 3, "There should be 3 drops, null values don't list")

    def test_get_results_basic_recursion(self):
        table1 = RDSTable()
        table2 = RDSTable()

        item1 = TestCaseItem("Green Cloth", always=True)
        item2 = TestCaseItem("Rusty Sword", always=True)
        item3 = TestCaseItem("Wet Mop", always=True)
        nullvalue = RDSNullValue(1) #probability is required
        nullvalue.rds_always = True

        table1.add_entry(item1)
        table1.add_entry(item2)
        table1.add_entry(nullvalue)
        table1.rds_always = True

        table2.add_entry(item2)
        table2.add_entry(item3)
        table2.add_entry(nullvalue)
        table2.rds_always = True

        self.rdstable.add_entry(table1)
        self.rdstable.add_entry(table2)

        self.assertEqual(len(self.rdstable.rds_result), 4, "There should be 4 drops, null values don't list")

    
    def test_get_results_5basic_random(self):
        # Set up the main table properties
        self.rdstable.rds_count = 5

        # Create items
        item1 = TestCaseItem("Armor", probability=5)
        item2 = TestCaseItem("Green cloth", probability=15) 
        item3 = TestCaseItem("Rock", probability=20)
        item4 = TestCaseItem("Cinnamon", probability=10)
        item5 = TestCaseItem("Rusty Knife", probability=17)
        item6 = TestCaseItem("Blood Ruby", probability=.5)

        #Add to table
        self.rdstable.add_entry(item1)
        self.rdstable.add_entry(item2)
        self.rdstable.add_entry(item3)
        self.rdstable.add_entry(item4)
        self.rdstable.add_entry(item5)
        self.rdstable.add_entry(item6)


        self.assertEqual(len(self.rdstable.rds_result), 5, "There should be 5 drops")



if __name__ == '__main__':
    unittest.main()


