# Table of Contents

* [dropgen](#dropgen)
* [dropgen.IRDSValue](#dropgen.IRDSValue)
  * [IRDSValue](#dropgen.IRDSValue.IRDSValue)
    * [rds\_value](#dropgen.IRDSValue.IRDSValue.rds_value)
    * [rds\_value](#dropgen.IRDSValue.IRDSValue.rds_value)
* [dropgen.RDSNullValue](#dropgen.RDSNullValue)
  * [RDSNullValue](#dropgen.RDSNullValue.RDSNullValue)
    * [\_\_init\_\_](#dropgen.RDSNullValue.RDSNullValue.__init__)
* [dropgen.RDSObject](#dropgen.RDSObject)
  * [RDSObject](#dropgen.RDSObject.RDSObject)
    * [\_\_init\_\_](#dropgen.RDSObject.RDSObject.__init__)
    * [on\_rds\_pre\_result\_eval](#dropgen.RDSObject.RDSObject.on_rds_pre_result_eval)
    * [on\_rds\_hit](#dropgen.RDSObject.RDSObject.on_rds_hit)
    * [on\_rds\_post\_result\_eval](#dropgen.RDSObject.RDSObject.on_rds_post_result_eval)
* [dropgen.RDSItem](#dropgen.RDSItem)
  * [RDSItem](#dropgen.RDSItem.RDSItem)
    * [\_\_init\_\_](#dropgen.RDSItem.RDSItem.__init__)
* [dropgen.IRDSObject](#dropgen.IRDSObject)
  * [IRDSObject](#dropgen.IRDSObject.IRDSObject)
    * [rds\_probability](#dropgen.IRDSObject.IRDSObject.rds_probability)
    * [rds\_probability](#dropgen.IRDSObject.IRDSObject.rds_probability)
    * [rds\_unique](#dropgen.IRDSObject.IRDSObject.rds_unique)
    * [rds\_unique](#dropgen.IRDSObject.IRDSObject.rds_unique)
    * [rds\_always](#dropgen.IRDSObject.IRDSObject.rds_always)
    * [rds\_always](#dropgen.IRDSObject.IRDSObject.rds_always)
    * [rds\_enabled](#dropgen.IRDSObject.IRDSObject.rds_enabled)
    * [rds\_enabled](#dropgen.IRDSObject.IRDSObject.rds_enabled)
    * [rds\_table](#dropgen.IRDSObject.IRDSObject.rds_table)
    * [rds\_table](#dropgen.IRDSObject.IRDSObject.rds_table)
    * [on\_rds\_pre\_result\_eval](#dropgen.IRDSObject.IRDSObject.on_rds_pre_result_eval)
    * [on\_rds\_hit](#dropgen.IRDSObject.IRDSObject.on_rds_hit)
    * [on\_rds\_post\_result\_eval](#dropgen.IRDSObject.IRDSObject.on_rds_post_result_eval)
* [dropgen.RDSTable](#dropgen.RDSTable)
  * [RDSTable](#dropgen.RDSTable.RDSTable)
    * [\_\_init\_\_](#dropgen.RDSTable.RDSTable.__init__)
    * [clear\_contents](#dropgen.RDSTable.RDSTable.clear_contents)
    * [add\_entry](#dropgen.RDSTable.RDSTable.add_entry)
    * [rds\_result](#dropgen.RDSTable.RDSTable.rds_result)
    * [on\_rds\_pre\_result\_eval](#dropgen.RDSTable.RDSTable.on_rds_pre_result_eval)
    * [on\_rds\_hit](#dropgen.RDSTable.RDSTable.on_rds_hit)
    * [on\_rds\_post\_result\_eval](#dropgen.RDSTable.RDSTable.on_rds_post_result_eval)
* [dropgen.RDSValue](#dropgen.RDSValue)
  * [RDSValue](#dropgen.RDSValue.RDSValue)
    * [\_\_init\_\_](#dropgen.RDSValue.RDSValue.__init__)
    * [on\_rds\_pre\_result\_eval](#dropgen.RDSValue.RDSValue.on_rds_pre_result_eval)
    * [on\_rds\_hit](#dropgen.RDSValue.RDSValue.on_rds_hit)
    * [on\_rds\_post\_result\_eval](#dropgen.RDSValue.RDSValue.on_rds_post_result_eval)
* [dropgen.IRDSTable](#dropgen.IRDSTable)
  * [IRDSTable](#dropgen.IRDSTable.IRDSTable)
    * [rds\_count](#dropgen.IRDSTable.IRDSTable.rds_count)
    * [rds\_count](#dropgen.IRDSTable.IRDSTable.rds_count)
    * [rds\_contents](#dropgen.IRDSTable.IRDSTable.rds_contents)
    * [rds\_contents](#dropgen.IRDSTable.IRDSTable.rds_contents)
    * [rds\_result](#dropgen.IRDSTable.IRDSTable.rds_result)
    * [rds\_result](#dropgen.IRDSTable.IRDSTable.rds_result)

<a name="dropgen"></a>
# dropgen

<a name="dropgen.IRDSValue"></a>
# dropgen.IRDSValue

<a name="dropgen.IRDSValue.IRDSValue"></a>
## IRDSValue Objects

```python
class IRDSValue(IRDSObject)
```

Use this in case of varying value of a particular 
item can be dropped.  i.e. gold coins

<a name="dropgen.IRDSValue.IRDSValue.rds_value"></a>
#### rds\_value

```python
 | @property
 | rds_value()
```

Get the value of an item

<a name="dropgen.IRDSValue.IRDSValue.rds_value"></a>
#### rds\_value

```python
 | @rds_value.setter
 | rds_value(rds_value)
```

Set the value of an item
requires: integer/double

<a name="dropgen.RDSNullValue"></a>
# dropgen.RDSNullValue

<a name="dropgen.RDSNullValue.RDSNullValue"></a>
## RDSNullValue Objects

```python
class RDSNullValue(RDSValue)
```

Allow for null/None objects to be part of the 
result set

<a name="dropgen.RDSNullValue.RDSNullValue.__init__"></a>
#### \_\_init\_\_

```python
 | __init__(probability=None)
```

The null object makes it possible to have no object
be returned from an iteration of the loot engine
* probability -- default value is 1

<a name="dropgen.RDSObject"></a>
# dropgen.RDSObject

<a name="dropgen.RDSObject.RDSObject"></a>
## RDSObject Objects

```python
class RDSObject(IRDSObject)
```

Generic implementation of the IRDSObject interface

<a name="dropgen.RDSObject.RDSObject.__init__"></a>
#### \_\_init\_\_

```python
 | __init__(probability=None, unique=None, always=None, enabled=None)
```

Possible parameters:
 * probability - number (default 1)
 * unique - boolean (default false)
 * always - boolean (default false)
 * enabled - boolean (default true)

<a name="dropgen.RDSObject.RDSObject.on_rds_pre_result_eval"></a>
#### on\_rds\_pre\_result\_eval

```python
 | on_rds_pre_result_eval(**kwargs)
```

Do nothing

<a name="dropgen.RDSObject.RDSObject.on_rds_hit"></a>
#### on\_rds\_hit

```python
 | on_rds_hit(**kwargs)
```

Do nothing

<a name="dropgen.RDSObject.RDSObject.on_rds_post_result_eval"></a>
#### on\_rds\_post\_result\_eval

```python
 | on_rds_post_result_eval(**kwargs)
```

Do nothing

<a name="dropgen.RDSItem"></a>
# dropgen.RDSItem

<a name="dropgen.RDSItem.RDSItem"></a>
## RDSItem Objects

```python
class RDSItem(RDSObject)
```

Create a basic item to add to a table.

<a name="dropgen.RDSItem.RDSItem.__init__"></a>
#### \_\_init\_\_

```python
 | __init__(name, probability=None, unique=None, always=None, enabled=None)
```

Requireed:
* name - What is item called?

Possible parameters:
* probability - number (default 1)
* unique - boolean (default false)
* always - boolean (default false)
* enabled - boolean (default true)

<a name="dropgen.IRDSObject"></a>
# dropgen.IRDSObject

<a name="dropgen.IRDSObject.IRDSObject"></a>
## IRDSObject Objects

```python
class IRDSObject(ABC)
```

This abstact class contains the properties, methods, etc an object must 
have to be a valid dropgen result object.

<a name="dropgen.IRDSObject.IRDSObject.rds_probability"></a>
#### rds\_probability

```python
 | @property
 | rds_probability()
```

Get probability
returns: integer/double

<a name="dropgen.IRDSObject.IRDSObject.rds_probability"></a>
#### rds\_probability

```python
 | @rds_probability.setter
 | rds_probability(rds_probability)
```

Set probability
requires: integer/double

<a name="dropgen.IRDSObject.IRDSObject.rds_unique"></a>
#### rds\_unique

```python
 | @property
 | rds_unique()
```

Get uniqueness -- -- drop only once?
returns: boolean

<a name="dropgen.IRDSObject.IRDSObject.rds_unique"></a>
#### rds\_unique

```python
 | @rds_unique.setter
 | rds_unique(rds_unique)
```

Set uniqueness -- drop only once?
requires: boolean

<a name="dropgen.IRDSObject.IRDSObject.rds_always"></a>
#### rds\_always

```python
 | @property
 | rds_always()
```

Get whether to always drop
returns: boolean

<a name="dropgen.IRDSObject.IRDSObject.rds_always"></a>
#### rds\_always

```python
 | @rds_always.setter
 | rds_always(rds_always)
```

Set whether to always drop
requires: boolean

<a name="dropgen.IRDSObject.IRDSObject.rds_enabled"></a>
#### rds\_enabled

```python
 | @property
 | rds_enabled()
```

Get whether to drop now
returns: boolean

<a name="dropgen.IRDSObject.IRDSObject.rds_enabled"></a>
#### rds\_enabled

```python
 | @rds_enabled.setter
 | rds_enabled(rds_enabled)
```

Set whether to drop now
requires: boolean

<a name="dropgen.IRDSObject.IRDSObject.rds_table"></a>
#### rds\_table

```python
 | @property
 | rds_table()
```

Get the table this Object belongs to
returns: object

<a name="dropgen.IRDSObject.IRDSObject.rds_table"></a>
#### rds\_table

```python
 | @rds_table.setter
 | rds_table(rds_table)
```

set the table this Object belongs to
requires: object

<a name="dropgen.IRDSObject.IRDSObject.on_rds_pre_result_eval"></a>
#### on\_rds\_pre\_result\_eval

```python
 | @abstractmethod
 | on_rds_pre_result_eval(**kwargs)
```

What if anything to do before the result is returned

<a name="dropgen.IRDSObject.IRDSObject.on_rds_hit"></a>
#### on\_rds\_hit

```python
 | @abstractmethod
 | on_rds_hit(**kwargs)
```

What if anything to do while the results are
being generated/returned.

<a name="dropgen.IRDSObject.IRDSObject.on_rds_post_result_eval"></a>
#### on\_rds\_post\_result\_eval

```python
 | @abstractmethod
 | on_rds_post_result_eval(**kwargs)
```

What if anything to do after the results 
have been received.

<a name="dropgen.RDSTable"></a>
# dropgen.RDSTable

<a name="dropgen.RDSTable.RDSTable"></a>
## RDSTable Objects

```python
class RDSTable(IRDSTable)
```

<a name="dropgen.RDSTable.RDSTable.__init__"></a>
#### \_\_init\_\_

```python
 | __init__(contents=None, count=None, probability=None, unique=None, always=None, enabled=None)
```

Initialize the table.  
Default values:
  * contents: empty list (members are expected to implement IRDSObject interface)
  * count: (Default - 1) How many items should be dropped
  * probability: (Default - 1) What is the probability of something being dropped from this table
  * unique: (Default - False) If set to true, any item in this table or subtables can only be dropped once
  * always: (Default - False) If set to true, an item will always drop. Probability disabled.
  * enabled: (Default - True) If set to True, the item has a chance to drop.

<a name="dropgen.RDSTable.RDSTable.clear_contents"></a>
#### clear\_contents

```python
 | clear_contents()
```

Create an empty list for the table.

<a name="dropgen.RDSTable.RDSTable.add_entry"></a>
#### add\_entry

```python
 | add_entry(entry, probability=None, unique=None, always=None, enabled=None)
```

Add an entry to the table. it can even have its own 
probability, unique, always and enabled settings

* Assumption:  The entry implements the IRDSObject interface OR
implements the methods/properties in IRDSObject

<a name="dropgen.RDSTable.RDSTable.rds_result"></a>
#### rds\_result

```python
 | @property
 | rds_result()
```

Generate and return the results.
return: iterable

<a name="dropgen.RDSTable.RDSTable.on_rds_pre_result_eval"></a>
#### on\_rds\_pre\_result\_eval

```python
 | on_rds_pre_result_eval(**kwargs)
```

Do nothing

<a name="dropgen.RDSTable.RDSTable.on_rds_hit"></a>
#### on\_rds\_hit

```python
 | on_rds_hit(**kwargs)
```

Do nothing

<a name="dropgen.RDSTable.RDSTable.on_rds_post_result_eval"></a>
#### on\_rds\_post\_result\_eval

```python
 | on_rds_post_result_eval(**kwargs)
```

Do nothing

<a name="dropgen.RDSValue"></a>
# dropgen.RDSValue

<a name="dropgen.RDSValue.RDSValue"></a>
## RDSValue Objects

```python
class RDSValue(IRDSValue)
```

<a name="dropgen.RDSValue.RDSValue.__init__"></a>
#### \_\_init\_\_

```python
 | __init__(probability, value, unique=None, always=None, enabled=None)
```

Initialize the value object. 
  * probability -- number (required)
  * value - object (required -- None is ok)
Optional values:
 * unique - boolean (default false)
 * always - boolean (default false)
 * enabled - boolean (default true)

<a name="dropgen.RDSValue.RDSValue.on_rds_pre_result_eval"></a>
#### on\_rds\_pre\_result\_eval

```python
 | on_rds_pre_result_eval(**kwargs)
```

Do nothing

<a name="dropgen.RDSValue.RDSValue.on_rds_hit"></a>
#### on\_rds\_hit

```python
 | on_rds_hit(**kwargs)
```

Do nothing

<a name="dropgen.RDSValue.RDSValue.on_rds_post_result_eval"></a>
#### on\_rds\_post\_result\_eval

```python
 | on_rds_post_result_eval(**kwargs)
```

Do nothing

<a name="dropgen.IRDSTable"></a>
# dropgen.IRDSTable

<a name="dropgen.IRDSTable.IRDSTable"></a>
## IRDSTable Objects

```python
class IRDSTable(IRDSObject)
```

Tables need to inherit from the this interface/abstract class in order to be 
used with the base implementation RDSTable

<a name="dropgen.IRDSTable.IRDSTable.rds_count"></a>
#### rds\_count

```python
 | @property
 | rds_count()
```

Get number of items to drop
returns: integer/double

<a name="dropgen.IRDSTable.IRDSTable.rds_count"></a>
#### rds\_count

```python
 | @rds_count.setter
 | rds_count(rds_count)
```

Set number of items to drop
requires: integer/double

<a name="dropgen.IRDSTable.IRDSTable.rds_contents"></a>
#### rds\_contents

```python
 | @property
 | rds_contents()
```

Get contents
return: iterable

<a name="dropgen.IRDSTable.IRDSTable.rds_contents"></a>
#### rds\_contents

```python
 | @rds_contents.setter
 | rds_contents(rds_contents)
```

Set contents

<a name="dropgen.IRDSTable.IRDSTable.rds_result"></a>
#### rds\_result

```python
 | @property
 | rds_result()
```

Get results
return: iterable

<a name="dropgen.IRDSTable.IRDSTable.rds_result"></a>
#### rds\_result

```python
 | @rds_result.setter
 | rds_result(rds_result)
```

Set contents

